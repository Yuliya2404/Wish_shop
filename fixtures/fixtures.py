import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture
def browser():
    chrome_browser = Chrome(executable_path=ChromeDriverManager().install())
    chrome_browser.maximize_window()
    chrome_browser.get("https://wish-shop.com.ua/")
    yield chrome_browser
    chrome_browser.quit()

