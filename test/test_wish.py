from time import sleep

from selenium.webdriver import Chrome


class TestWish:
    def test_search_kulon(self, browser: Chrome):
        target_url = f"https://wish-shop.com.ua/"
        wish_title = f"Интернет магазин украшений со смыслом."
        assert browser.current_url == target_url, f"Incorrect url {browser.current_url}, should be {target_url}"
        assert browser.title == wish_title, f"Incorrect title {browser.title}, should be {wish_title}"

        search_field = browser.find_element_by_name("search")
        search_button = browser.find_element_by_css_selector("[class='btn']")
        search_field.send_keys("кулон")
        search_button.click()

        found_products = browser.find_elements_by_css_selector("[class='product-thumb']")
        assert len(found_products)

        for products in found_products:
            assert products.is_displayed(), f"Items are not displayed on the page"
            found_links = products.find_elements_by_xpath("//*[@class='caption']//h4//a")
            for links in found_links:
                assert "wish-shop" in links.get_attribute("href"), f"No links found"

    def test_facebook(self, browser: Chrome):
        target_url = f"https://wish-shop.com.ua/"
        wish_title = f"Интернет магазин украшений со смыслом."
        assert browser.current_url == target_url, f"Incorrect url {browser.current_url}, should be {target_url}"
        assert browser.title == wish_title, f"Incorrect title {browser.title}, should be {wish_title}"

        search_button = browser.find_element_by_xpath("//a[contains(@href, 'facebook')]")
        search_button.click()
        window_facebook = browser.window_handles[1]
        browser.switch_to.window(window_facebook)
        print(browser.current_url.title())
        assert browser.current_url == "https://www.facebook.com/brasletgelaniy", f"Facebook is not opened"
        assert browser.current_url.title() == "Https://Www.Facebook.Com/Brasletgelaniy", f"Wrong title"

    def test_instagram(self, browser: Chrome):
        target_url = f"https://wish-shop.com.ua/"
        wish_title = f"Интернет магазин украшений со смыслом."
        assert browser.current_url == target_url, f"Incorrect url {browser.current_url}, should be {target_url}"
        assert browser.title == wish_title, f"Incorrect title {browser.title}, should be {wish_title}"

        search_button = browser.find_element_by_xpath("//a[contains(@href, 'instagram')]")
        search_button.click()
        window_instagram = browser.window_handles[1]
        browser.switch_to.window(window_instagram)
        print(browser.current_url.title())
        assert browser.current_url == "https://www.instagram.com/braslet_gelaniy/", f"Instagram is not opened"
        assert browser.current_url.title() == "Https://Www.Instagram.Com/Braslet_Gelaniy/", f"Wrong title"







